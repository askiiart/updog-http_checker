# Updog HTTP checker

An HTTP checker for updog.

## Config

```json
{
    "logging": "HttpChecker",
    "logging-args": {
        "url": "https://url.to.be.checked.net"
    }
}
```

## Arguments

Arguments marked with * are required.

### `url`*

The URL for the thing being checked.

Default: None, this is a required value.

#### Example

```json
"url": "https://example.com:443"
```

### `append-messages`

Text to be appended after the status message for each status code.

TODO: Add ranges and lists for these - will not be stored like that, just processed into their currently format.

Default: None

#### Example

```json
"append-messages": {
    "200": " - Up",
    "404": " - Partial outage",
    "502": " - Down"
}
```

### `headers`

Headers to be sent for the HTTP request.

Default: None

#### Example

```json
"headers": {
    "Content-Type": "text"
}
```
