import requests
from http.client import responses


class HttpChecker:
    def __init__(self, arguments):
        self.url = arguments['url']
        # return an empty dict for optional things
        append_messages = arguments.get(
            'append-messages', dict())
        self.headers = arguments.get('headers', dict())

        # Append the custom stuff to the response status code messages
        self.status_messages = responses
        for status_code in self.status_messages:
            self.status_messages[status_code] = str(
                self.status_messages[status_code]) + append_messages.get(str(status_code), '')

    def get_status(self):
        return requests.get(self.url, headers=self.headers).status_code

    def get_return_codes(self):
        return (self.status_messages)
